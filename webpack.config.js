const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
  entry: {
    app: {import:'./src/index.js', filename:'public/build.min.js'},
    opt: {import:'./opt/index.js', filename:'public/opt.min.js'},
  },
  output: {
    path: __dirname,
  },
  module: {},
  optimization: {
    minimizer: [new TerserPlugin({
      extractComments: false
    })]
  },
  devServer: {
    historyApiFallback: true,
    allowedHosts: "all",
    static: {
      directory: __dirname,
      publicPath: '/'
    }
  },
  resolve: {
    fallback:{
      "fs":false
    },
  }
};
