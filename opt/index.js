/* global THREE */

require('aframe-event-set-component');
require('aframe-globe-component');

require('./utils/math/index');
require('./diagrams/index');
require('./particles/index')

require('./grab-ownership.component');
require('./controller-tracking-grab-controls.component');
require('./hand-track-grab-controls-debug.component')
require('./onconnect');
require('./synth/index');