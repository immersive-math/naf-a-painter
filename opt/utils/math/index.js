function castToVec3(aVec3){
    return(new THREE.Vector3(aVec3.x, aVec3.y, aVec3.z));
}

/**
 * Returns an elmenets position represented as a Three.Vector3
 * @param el aframe element
 * @returns {*} Three.Vector3 local position
 */
function getVec3position(el){
    const pos = el.getAttribute('position');
    return(new THREE.Vector3(pos.x, pos.y, pos.z));
}


/**
 * Converts aframe element rotation attribute to THREE.Quaternion
 * @param el aframe element
 * @returns {*} THREE.Quaternion
 */
function getQuaternionFromRotation(el) {
    const rot = el.getAttribute('rotation');
    const euler = new THREE.Euler(
        THREE.MathUtils.degToRad(rot.x),
        THREE.MathUtils.degToRad(rot.y),
        THREE.MathUtils.degToRad(rot.z)
    );
    return new THREE.Quaternion().setFromEuler(euler);
}

/**
 * Checks if two Three.Vector3 are qual
 * @param a Three.Vector3 first comparison
 * @param b Three.Vector3 second comparison
 * @returns {boolean} bool is equal
 */
function isEqualVec3 (a, b) {
    if (!a || !b) { return false; }
    return (a.x === b.x && a.y === b.y && a.z === b.z);
}

/**
 * Calculates the closest position on a line from a point (as an aframe element)
 * @param point an aframe elemnt, reference external to line
 * @param lineStartPoint an aframe elemnet, point that exists on the line
 * @param lineDir a Three.Vector3, direction of the line
 * @returns {*} Three.Vector3 position
 */
function getClosestVec3OnLineFromPoint(point, lineStartPoint, lineDir){
    return getClosestVec3OnLine(getVec3position(point), getVec3position(lineStartPoint), lineDir);
}

/**
 * Finds closest position on a line from a point
 * @param vec3 reference point as Three.Vector3
 * @param lineStart point on line as Three.Vector3
 * @param lineDir direction of line sa Three.Vector3
 * @returns {*} cloest point to reference on line as Three.Vector3
 */
function getClosestVec3OnLine(vec3, lineStart, lineDir){
    const delta = vec3.clone().sub(lineStart);
    return (lineStart.clone().add(delta.projectOnVector(lineDir)));
}

/**
 * Find direction between two points (aframe elements)
 * @param pointa aframe element
 * @param pointb aframe element
 * @returns {*} Three.Vector3 normalized displacement
 */
function getVec3DirectionFromPoints(pointa, pointb){
    const posa = getVec3position(pointa);
    const posb = getVec3position(pointb);
    return(posa.clone().sub(posb).normalize());
}

/**
 * Finds the midpoint on a segment between two points (aframe elements)
 * @param pointa a frame element, endpoint
 * @param pointb aframe elemnet, endpoint
 * @returns {*} Three.Vector3 location
 */
function getVec3midpointFromPoints(pointa, pointb){
    const posa = getVec3position(pointa);
    const posb = getVec3position(pointb);
    return(posa.add(posb.clone().sub(posa).normalize().multiplyScalar(0.5)));
}

/***
 * Finds a point as a ratio of the displacement between two locations (as Three.Vector3)
 * @param start Three.Vector3 location
 * @param end Three.Vector3 location
 * @param portion float ratio
 * @returns {*} Three.Vector3 location on line
 */
function getProprotionalVec3(start, end, portion){
    let res = end.clone()
    res.sub(start).multiplyScalar(portion).add(start);
    return(res);
}

function projectParallelToFloor(el, height){
    let pos = el.getAttribute('position');
    pos.y = height;
    return pos;
}

module.exports = {
    castToVec3,
    getVec3position,
    getQuaternionFromRotation,
    isEqualVec3,
    getClosestVec3OnLine,
    getClosestVec3OnLineFromPoint,
    getVec3DirectionFromPoints,
    getVec3midpointFromPoints,
    getProprotionalVec3,
    projectParallelToFloor
}