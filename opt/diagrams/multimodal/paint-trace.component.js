AFRAME.registerComponent('paint-trace', {
    depends: ['brush'],
    schema: {
        color: {type: 'color', default: 'blue'}
    },
    init: function(){
        this.el.addEventListener('start-painting', this.startPaint);
        this.el.addEventListener('stop-painting', this.stopPaint);
    },
    startPaint: function(event){
    //trace with painting tools
        this.el.setAttribute('brush', 'color', event.detail.color);
        this.el.emit('paint', {value: 1});
    },
    stopPaint: function(event){
    //trace with painting tools
        this.el.setAttribute('brush', 'color', event.detail.color);
        this.el.emit('paint', {value: 0});
    }
});