AFRAME.registerComponent('angle-synth', {
    depends: ['polysynth'],
    schema: {
        rhythm: {type:'array', default: [250, 250, 250, 500, 250, 500]},
        rhythmIdx: {type:'number', default: 0},
        value: {type: 'number'},
        target: {type:'number'},
        enabled:{type:'boolean', default:false}
    },
    init: function(){
        let data = this.data;  // Component property values.
        data.currentTime = Date.now();
        data.elapsedTime = 0;
        this.el.sceneEl.addEventListener('enter-vr', e => {this.data.enabled = true});
    },
    tick: function(){
        let data = this.data;

        if(data.enabled) {
            data.elapsedTime = Date.now() - data.currentTime;

            if (data.elapsedTime > data.rhythm[data.rhythmIdx]) {
                this.el.setAttribute('polysynth',
                    {notes: this.chordMap(data.value, 3)})
                this.el.emit('polysynth-tone')
                data.elapsedTime = 0
                data.currentTime = Date.now();
                data.rhythmIdx = (data.rhythmIdx + 1) % data.rhythm.length;
            }
        }
    },
    noteMap: function(angles){
        let returnArray = []
        const noteArray = ['C3', 'D3', 'D#3', 'F3', 'G3', 'A3', 'A#3', 'C4', 'D4', 'D#4', 'F4', 'G4', 'A4', 'A#4'];
        for (let i = 0; i < angles.length; i++){
            let angleToIdx = Math.floor((angles[i]/Math.PI)*14);
            returnArray[i] = noteArray[angleToIdx]
        }
        return returnArray;
    },
    chordMap: function(angle, numberOfNotes){
        let returnArray = []
        const noteArray = ['C3', 'D3', 'D#3', 'F3', 'G3', 'A3', 'A#3', 'C4', 'D4', 'D#4', 'F4', 'G4', 'A4', 'A#4'];
        let angleToIdx = Math.floor((angle/Math.PI) * noteArray.length-(numberOfNotes-1));
        for (let i = 0; i < numberOfNotes; i++){
            returnArray[i] = noteArray[angleToIdx+i]
        }
        return returnArray;
    },
});

Array.prototype.sample = function(){
    return this[Math.floor(Math.random()*this.length)];
}