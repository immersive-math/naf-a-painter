AFRAME.registerComponent('spectator-above',{
    schema: {
    },
    init: function(){
        //setup spectator camera
        let spectatorCamera = document.querySelector('#spectator-camera');
        spectatorCamera.setAttribute('position', "0 2.5 0");
        spectatorCamera.setAttribute('rotation', "-90 0 0");
        spectatorCamera.setAttribute('look-controls', 'enabled', false);
        spectatorCamera.setAttribute('wasd-controls', 'enabled', false);
    }
});