AFRAME.registerComponent('conditional-haptics', {
    schema:{
    },
    init: function(){
        this.el.addEventListener('conditional-haptic-feedback', this.feedback);
        this.currentTime = Date.now();
        this.elapsedTime = 0;
    },
    feedback: function(event){
        this.elapsedTime = Date.now() - this.currentTime;
        if(this.elapsedTime > 100){
            this.currentTime = Date.now();

            let haptic_els = this.el.sceneEl.querySelectorAll('[haptics]');
            //i = 1 to skip mixin
            for(let i=1; i< haptic_els.length; i++){
                haptic_els[i].component.haptics.pulse(event.detail.value, 100);
            }
        }
    }
});