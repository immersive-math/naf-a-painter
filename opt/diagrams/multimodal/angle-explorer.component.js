const mathVec3 = require('/opt/utils/math/index');

// const log = require('log-to-file');
//is this actually a system??
AFRAME.registerComponent('angle-explorer', {
    dependencies: ['angle-synth', 'conditional-haptics', 'spectator-above', 'shearing-triangle'],
    schema:{
        pointa: {type:'selector'},
        pointb: {type:'selector'},
        apex: {type:'selector'},
        control1: {type:'selector'},
        control2: {type:'selector'},
        control3: {type:'selector'},
        targetAngle: {type:'number'},
        tolerance: {type:'number', default: 0.01},
        meetsConditions: {type:'boolean'},
        // logfile: {type: 'string', default: '~/Downloads/diagram.log'},
        height: {type: 'number', default: 1.2},
        color: {type: 'color', default: 'blue'}
    },
    /**
     * Initialiation function
     */
    init: function () {
        let data = this.data;  // Component property values.
        data.targetAngle = this.calculateAngle(data.apex, data.pointa, data.pointb);
        data.apex.setAttribute('scale', {x: 0.025, y:0.025, z:0.025});
        //this.el.sceneEl.querySelector('#environment-root').setAttribute('environment', 'active', false);
        //window.addEventListener('keydown', this.onKeyDown);
    },
    /**
     * grabhold called when a controller or other user is grasping or releasing one of the defining points
     */
    tick: function () {
        let data = this.data;
        //move points to match controls
        if(this.el.sceneEl.is('vr-mode')) {
            if (data.control1) {
                data.apex.setAttribute('position', mathVec3.projectParallelToFloor(data.control1, data.height));
                data.apex.emit('grab-hold');
            }
            if (data.control2) {
                data.pointa.setAttribute('position', mathVec3.projectParallelToFloor(data.control2, data.height));
                data.pointa.emit('grab-hold');
            }
            if (data.control3) {
                data.pointb.setAttribute('position', mathVec3.projectParallelToFloor(data.control3, data.height));
                data.pointb.emit('grab-hold');
            }
        }
        this.feedback(this.data);
        // this.log(this.data);
    },
    onKeyDown: function(e){
        if(e.key == 'r') {
            let data = this.data;
            let angle = this.calculateAngle(data.apex, data.pointa, data.pointb);
            data.targetAngle = angle;
        }
    },
    feedback: function(data) {
        let angle = this.calculateAngle(data.apex, data.pointa, data.pointb);
        this.el.setAttribute('angle-synth', 'value', angle);

        // check proximity of angles in radians
        if (Math.abs(data.targetAngle - angle) < data.tolerance) {
            //color
            if(!data.meetsConditions) {
                data.color = 'green';
                this.el.setAttribute('material', {color: data.color});
                data.meetsConditions = true;
                this.el.emit('stop-painting', {color: data.color});
                this.el.emit('start-painting', {color: data.color});
            }
            const strength = (1- (Math.abs(data.targetAngle - angle)/Math.PI))/Math.PI;
            this.el.emit('conditional-haptic-feedback', {value: strength});
        }else
        {
            if(data.meetsConditions) {
                data.color = 'red';
                this.el.setAttribute('material', {color: data.color})
                data.meetsConditions = false;
                this.el.emit('stop-painting', {color: data.color});
                this.el.emit('start-painting', {color: data.color});
            }
        }
    },
    calculateAngle: function(pointMiddle, pointA, pointB){
        const legA = mathVec3.getVec3position(pointA).sub(mathVec3.getVec3position(pointMiddle));
        const legB = mathVec3.getVec3position(pointB).sub(mathVec3.getVec3position(pointMiddle));

        return(legA.angleTo(legB));
    },
});