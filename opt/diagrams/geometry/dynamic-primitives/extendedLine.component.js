const mathVec3 = require('/opt/utils/math/index');
// TODO: this is written more like a geometry, entirely dependent on other objects for definition of point and direction. It might be ideal of the point and direction data to be updated within the component by position data from points in the scene.
/**
 * Extended line takes the line buffer geometry and renders a series of line segments to approximate a linee
 */
AFRAME.registerComponent('extended-line', {
    schema:  {
        point: {default: {x:0, y:0.5, z:0}, type: 'vec3'},
        direction: {default: {x:0, y:0.5, z:1}, type:'vec3'},
        nSegments: {default: 3, min:1},
        segmentLength: {default: 1, min: 0.01},
        color: {type: 'color', default: '#74BEC1'},
        opacity: {type: 'number', default: 1},
        visible: {default: true}
    },
    /**
     * Initialization function
     */
    init: function () {
        let data = this.data;
        this.rendererSystem = this.el.sceneEl.systems.renderer;
        let material = this.material = new THREE.LineBasicMaterial({
            color: data.color,
            opacity: data.opacity,
            transparent: data.opacity < 1,
            visible: data.visible
        });
        let lineGeometry = new THREE.BufferGeometry();
        lineGeometry.setAttribute( 'position', new THREE.Float32BufferAttribute((data.nSegments+1)*3,3));
        this.geometry = this.modifyLineGeometry(lineGeometry, data);
        this.rendererSystem.applyColorCorrection(material.color);
        this.line = new THREE.Line(lineGeometry, material);
        this.el.setObject3D(this.attrName, this.line);
    },
    /**
     * Update once per frame, check if updated is needed then call to modify line geometry
     * @param oldData old ECS data
     */
    update: function (oldData) {
        // TODO tick is more aggressive than necessary
        let data = this.data;
        let geometry = this.geometry;
        let material = this.material;
        let geoNeedsUpdate =(!mathVec3.isEqualVec3(data.direction, oldData.direction)) ||(!mathVec3.isEqualVec3(data.point, oldData.point));

        if (geoNeedsUpdate) {
            this.modifyLineGeometry(geometry, data);
            geometry.attributes.position.needsUpdate = true;
            geometry.computeBoundingSphere();
        }
        material.color.setStyle(data.color);
        this.rendererSystem.applyColorCorrection(material.color);
        material.opacity = data.opacity;
        material.transparent = data.opacity < 1;
        material.visible = data.visible;
    },
    /**
     * removing this element removes the object3d
     */
    remove: function () {
        this.el.removeObject3D(this.attrName, this.line);
    },
    /**
     * rewrite vertices for line buffer geometry
     * @param lineGeometry existing line buffer geometry
     * @param data ECS buffer geometry
     * @returns {*} new line buffer geometry with modified positions
     */
    modifyLineGeometry: function(lineGeometry, data){
        const direction = new THREE.Vector3(data.direction.x, data.direction.y, data.direction.z);
        for(let i=0; i< data.nSegments+1; i++){
            const pos = this.calculatePosition(data, i, direction);
            lineGeometry.attributes.position.setXYZ(i, pos.x, pos.y, pos.z);
        }
        return lineGeometry;
    },
    /**
     * calculates the ith position for the line
     * @param data ECS data
     * @param index int index
     * @param direction three.Vector3 direction of line
     * @returns {*} Three.Vector3 position
     */
    calculatePosition(data, index, direction){
        const segmentDisplacement = direction.clone().multiplyScalar(data.nSegments*data.segmentLength);
        const proportionFromCenter = 0.5-(index/data.nSegments);
        return data.point.clone().add(segmentDisplacement.clone().multiplyScalar(proportionFromCenter));
    }
});