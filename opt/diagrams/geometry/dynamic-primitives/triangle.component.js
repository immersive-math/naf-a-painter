const mathVec3 = require('/opt/utils/math/index');

/**
 * a component for a dynamically rendered triangle, drawn between three points
 */
AFRAME.registerComponent('dynamic-triangle', {
    schema:{
        pointa: {type:'selector'},
        pointb: {type:'selector'},
        pointc: {type:'selector'}
    },
    /**
     * Initialiation function
     */
    init: function () {
        let data = this.data;  // Component property values.
        this.calculateGeometry(this.data)

        //TODO refactor to loop
        data.pointa.addEventListener('grab-hold', e => {this.grabhold(e)});
        data.pointa.addEventListener('grab-end', e => {this.grabhold(e)});
        data.pointb.addEventListener('grab-hold', e => {this.grabhold(e)});
        data.pointb.addEventListener('grab-end', e => {this.grabhold(e)});
        data.pointc.addEventListener('grab-hold', e => {this.grabhold(e)});
        data.pointc.addEventListener('grab-end', e => {this.grabhold(e)});
    },
    /**
     * grabhold called when a controller or other user is grasping or releasing one of the defining points
     */
    grabhold: function () {
        this.calculateGeometry(this.data)
    },
    /**
     * reassigns data on extended line visualizations to reflect state of aframe elements
     */
    calculateGeometry: function(data){
        this.el.setAttribute('geometry',{vertexA:mathVec3.getVec3position(data.pointa),vertexB:mathVec3.getVec3position(data.pointb),vertexC:mathVec3.getVec3position(data.pointc)});
    }
});