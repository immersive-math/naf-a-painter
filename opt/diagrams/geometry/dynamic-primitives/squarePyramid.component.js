const mathVec3 = require('/opt/utils/math/index');
/**
 * A dynamically rendered square-based pyramid
 */
AFRAME.registerComponent('square-pyramid',{
    schema:{
        //a,b,c,d make up base in clockwise order
        pointa: {type:'selector'},
        pointb: {type:'selector'},
        pointc: {type:'selector'},
        pointd: {type:'selector'},
        pointe: {type:'selector'},
        color: {type:'color', default:'red'}
    },
    /**
     * Initialization, create dynaimc triangles for faces
     */
    init: function(){
        let data = this.data;  // Component property values.
        let el = this.el;  // Reference to the component's entity.

        //square base
        el.appendChild(this.instantiateDynamicTriangle(data.pointa,data.pointb,data.pointc,data.color))
        el.appendChild(this.instantiateDynamicTriangle(data.pointc,data.pointa,data.pointd,data.color))

        //triangular sides
        el.appendChild(this.instantiateDynamicTriangle(data.pointa,data.pointb,data.pointe,data.color))
        el.appendChild(this.instantiateDynamicTriangle(data.pointb,data.pointc,data.pointe,data.color))
        el.appendChild(this.instantiateDynamicTriangle(data.pointc,data.pointd,data.pointe,data.color))
        el.appendChild(this.instantiateDynamicTriangle(data.pointd,data.pointa,data.pointe,data.color))
    },
    /**
     * Abstraction of triangle instance
     * @param pointa vertex1 as string tag for aframe element
     * @param pointb vertex2 as string tag for aframe element
     * @param pointc vertex3 as as string tag for aframe element
     * @param color a string argument for color
     * @returns {HTMLElement} face element
     */
    instantiateDynamicTriangle: function(pointa, pointb, pointc, color) {
        let triangle_el = document.createElement('a-triangle');
        triangle_el.setAttribute('dynamic-triangle', {
            pointa: '#' + pointa.id,
            pointb: '#' + pointb.id,
            pointc: '#' + pointc.id
        });
        triangle_el.setAttribute('material', {side: "double", color: color, transparent:true, opacity: 0.7});
        return triangle_el;
    }
});