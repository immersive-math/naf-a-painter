const mathVec3 = require('/opt/utils/math/index');
/**
 * Component to construct a pair of parallel lines, constructed from three points (aframe elements)
 */
AFRAME.registerComponent('dynamic-line', {
    schema:{
        pointa: {type:'selector'},
        pointb: {type:'selector'},
        line: {type:'string'},
        length:{type:'number', default: 1},
        nSegments:{type:'number', default:2000},
        dynamic:{type:'bool', default:false}
    },
    /**
     * Intialization function
     * builds lines as child objects
     */
    init: function(){
        let data = this.data;  // Component property values.
        let el = this.el;  // Reference to the component's entity.

        let line1 = document.createElement('a-entity');

        el.appendChild(line1);

        line1.id = (el.id + "__l1");

        data.line1 = line1.id;

        document.getElementById(data.line1).setAttribute('extended-line', {
            nSegments: data.nSegments,
            segmentLength: data.length,
            color: 'white'
        });

        this.calculateGeometry(this.data);

        if(data.dynamic){
            //TODO refactor to loop
            data.pointa.addEventListener('grab-hold', e => {this.grabhold(e)});
            data.pointa.addEventListener('grab-end', e => {this.grabhold(e)});
            data.pointb.addEventListener('grab-hold', e => {this.grabhold(e)});
            data.pointb.addEventListener('grab-end', e => {this.grabhold(e)});
        }
    },
    /**
     * grabhold called when a controller or other user is grasping or releasing one of the defining points
     */
    grabhold: function () {
        this.calculateGeometry(this.data);
    },
    /**
     * reassigns data on extended line visualizations to reflect state of aframe elements
     * @param data ECS data
     */
    calculateGeometry: function(data){
        const direction = mathVec3.getVec3position(data.pointa).sub(mathVec3.getVec3position(data.pointb)).normalize();
        document.getElementById(data.line1).setAttribute('extended-line', {point: mathVec3.getVec3position(data.pointa), direction: direction});
    }
});