const mathVec3 = require('/opt/utils/math/index');

/**
 * Shearing constraints for triangles
 */
AFRAME.registerComponent('shearing-trinagle', {
    schema:{
        apex: {type:'selector'},
        pointa: {type:'selector'},
        pointb: {type:'selector'},
        linePos: {type: 'vec3', default: {x: 0, y: 1, z: 0}},
        linePos2: {type:'vec3', default: {x:0, y:1, z:0}},
        lineDir: {type: 'vec3', default: {x: 0, y: 1, z: 0}}
    },
    init: function(){
        //TODO generate lineDir here
        let data = this.data;  // Component property values.
        const linePosVec3 = mathVec3.getVec3position(data.apex);
        data.linePos = {x: linePosVec3.x, y: linePosVec3.y, z: linePosVec3.z};
        const linePos2Vec3 = mathVec3.getVec3position(data.pointa);
        data.linePos2 = {x: linePos2Vec3.x, y: linePos2Vec3.y, z: linePos2Vec3.z};

        const linePointAVec3 = mathVec3.getVec3position(data.pointa);
        const linePointBVec3 = mathVec3.getVec3position(data.pointb);
        const lineDirVec3 = linePointAVec3.sub(linePointBVec3)
        data.lineDir = {x: lineDirVec3.x, y:lineDirVec3.y, z:lineDirVec3.z}
        this.grabhold = this.grabhold.bind(this);

        data.apex.addEventListener('grab-hold', e => {this.grabhold(e)});
        data.apex.addEventListener('grab-end', e => {this.grabhold(e)});
        data.pointa.addEventListener('grab-hold', e => {this.grabhold(e)});
        data.pointa.addEventListener('grab-end', e => {this.grabhold(e)});
        data.pointb.addEventListener('grab-hold', e => {this.grabhold(e)});
        data.pointb.addEventListener('grab-end', e => {this.grabhold(e)});
    },
    grabhold(e) {
        const data = this.data;
        if(e.target.id === data.apex.id) {
            const linePosVec3 = new THREE.Vector3(data.linePos.x, data.linePos.y, data.linePos.z);
            const lineDirVec3 = new THREE.Vector3(data.lineDir.x, data.lineDir.y, data.lineDir.z);
            const diffVec3 = (mathVec3.getVec3position(data.apex).sub(linePosVec3)).projectOnVector(lineDirVec3);
            const newPosVec3 = linePosVec3.add(diffVec3);
            data.apex.setAttribute('position', {x: newPosVec3.x, y: newPosVec3.y, z: newPosVec3.z});
        }
        if(e.target.id === data.pointa.id) {
            const linePosVec3 = new THREE.Vector3(data.linePos2.x, data.linePos2.y, data.linePos2.z);
            const lineDirVec3 = new THREE.Vector3(data.lineDir.x, data.lineDir.y, data.lineDir.z);
            const diffVec3 = (mathVec3.getVec3position(data.pointa).sub(linePosVec3)).projectOnVector(lineDirVec3);
            const newPosVec3 = linePosVec3.add(diffVec3);
            data.pointa.setAttribute('position', {x: newPosVec3.x, y: newPosVec3.y, z: newPosVec3.z});
        }
        if(e.target.id === data.pointb.id) {
            const linePosVec3 = new THREE.Vector3(data.linePos2.x, data.linePos2.y, data.linePos2.z);
            const lineDirVec3 = new THREE.Vector3(data.lineDir.x, data.lineDir.y, data.lineDir.z);
            const diffVec3 = (mathVec3.getVec3position(data.pointb).sub(linePosVec3)).projectOnVector(lineDirVec3);
            const newPosVec3 = linePosVec3.add(diffVec3);
            data.pointb.setAttribute('position', {x: newPosVec3.x, y: newPosVec3.y, z: newPosVec3.z});
        }
    }
});