const mathVec3 = require('/opt/utils/math/index');

/**
 * Shearing constraints for pyramids
 */
AFRAME.registerComponent('shearing-cube', {
    schema: {
        apex: {type: 'selector'},
        pointa: {type: 'selector'},
        pointb: {type: 'selector'},
        pointc: {type: 'selector'},
        pointd: {type: 'selector'},
        pointe: {type: 'selector'},
        pointf: {type: 'selector'},
        pointg: {type: 'selector'},
        pointh: {type: 'selector'},
        xMin: {type:'number', default:0},
        yMin: {type:'number', default:0},
        zMin: {type:'number', default:0},
        xMax: {type:'number', default:0},
        yMax: {type:'number', default:0},
        zMax: {type:'number', default:0}
    },
    init: function(){
        let data = this.data;
        this.grabhold = this.grabhold.bind(this);
        data.apex.addEventListener('grab-hold', e => {this.grabhold(e)});
        data.apex.addEventListener('grab-end', e => {this.grabhold(e)});

        const pointavec3 = mathVec3.getVec3position(data.pointa);
        data.xMin = pointavec3.x;
        data.xMax = pointavec3.x;
        data.yMin = pointavec3.y;
        data.yMax = pointavec3.y;
        data.zMin = pointavec3.z;
        data.zMax = pointavec3.z;

        this.setCubeBoundaries(data.pointb);
        this.setCubeBoundaries(data.pointc);
        this.setCubeBoundaries(data.pointd);
        this.setCubeBoundaries(data.pointe);
        this.setCubeBoundaries(data.pointf);
        this.setCubeBoundaries(data.pointg);
        this.setCubeBoundaries(data.pointh);
    },
    /**
     * On grabhold of the apex, map apex into plane
     * @param e held element, check if this is the apex
     */
    grabhold(e) {
        const data = this.data;
        if(e.target.id === data.apex.id) {
            const apexPos = mathVec3.getVec3position(data.apex);
            const newApexPos = this.mapWithinCubeBounds();
            if(!mathVec3.isEqualVec3(apexPos, newApexPos)){
                data.apex.setAttribute('position', {x: newApexPos.x, y: newApexPos.y, z: newApexPos.z});
            }
            //TODO if apex is on boundary, make that pyramid invisible (up to 3).
        }
    },
    setCubeBoundaries: function(pointEl){
        let data = this.data;
        let vec3 = mathVec3.getVec3position(pointEl);
        if(data.xMin > vec3.x){
            data.xMin = vec3.x;
        }else if(data.xMax < vec3.x){
            data.xMax = vec3.x;
        }

        if(data.yMin > vec3.y){
            data.yMin = vec3.y;
        }else if(data.yMax < vec3.y){
            data.yMax = vec3.y;
        }

        if(data.zMin > vec3.z){
            data.zMin = vec3.z;
        }else if(data.zMax < vec3.z){
            data.zMax = vec3.z;
        }
    },
    mapWithinCubeBounds: function(){
        let data = this.data;
        let vec3 = mathVec3.getVec3position(data.apex);
        if(data.xMin > vec3.x){
            vec3.x = data.xMin ;
        }else if(data.xMax < vec3.x){
            vec3.x = data.xMax;
        }

        if(data.yMin > vec3.y){
            vec3.y = data.yMin;
        }else if(data.yMax < vec3.y){
            vec3.y = data.yMax;
        }

        if(data.zMin > vec3.z){
            vec3.z = data.zMin;
        }else if(data.zMax < vec3.z){
            vec3.z = data.zMax;
        }
        return(vec3)
    }
});