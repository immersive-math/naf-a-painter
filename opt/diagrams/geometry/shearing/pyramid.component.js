const mathVec3 = require('/opt/utils/math/index');

AFRAME.registerComponent('shearing-pyramid', {
    schema:{
        apex: {type:'selector'},
        planePos: {type: 'vec3', default: {x: 0, y: 1, z: 0}},
        planeNorm: {type: 'vec3', default: {x: 0, y: 1, z: 0}},
    },
    /**
     * Initialization, map grabhold on apex to update shearing
     */
    init: function(){
        let data = this.data;  // Component property values.
        const planePosVec3 = mathVec3.getVec3position(data.apex);
        data.planePos = {x: planePosVec3.x, y: planePosVec3.y, z: planePosVec3.z};
        this.grabhold = this.grabhold.bind(this);
        data.apex.addEventListener('grab-hold', e => {this.grabhold(e)});
        data.apex.addEventListener('grab-end', e => {this.grabhold(e)});
    },
    /**
     * On grabhold of the apex, map apex into plane
     * @param e held element, check if this is the apex
     */
    grabhold(e) {
        const data = this.data;
        if(e.target.id === data.apex.id) {
            const planePosVec3 = new THREE.Vector3(data.planePos.x, data.planePos.y, data.planePos.z);
            const planeNormVec3 = new THREE.Vector3(data.planeNorm.x, data.planeNorm.y, data.planeNorm.z);
            const diffVec3 = (mathVec3.getVec3position(data.apex).sub(planePosVec3)).projectOnPlane(planeNormVec3);
            const apexPosVec3 = planePosVec3.add(diffVec3);
            data.apex.setAttribute('position', {x: apexPosVec3.x, y: apexPosVec3.y, z: apexPosVec3.z});
        }
    }
});