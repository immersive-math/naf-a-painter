const mathVec3 = require('/opt/utils/math/index');

/**
 * A surface generated as a trace of the closeest distance between two line[segments]
 */
AFRAME.registerGeometry('lineInterpolationSurface', {
    schema: {
        line1point1: {default: {x:0, y:0.5, z:0}, type: 'vec3'},
        line1point2: {default: {x:0, y:0.5, z:1}, type:'vec3'},
        line2point1: {default: {x:1, y:0.5, z:0}, type: 'vec3'},
        line2point2: {default: {x:1, y:0.5, z:1}, type:'vec3'},
        linearHueMap: {default: false},
        n: {default: 10, min: 1}
    },
    /**
     * initialization function
     * @param data ECS data
     */
    init: function (data) {
        let planeGeometry = new THREE.PlaneBufferGeometry(1,1,1,data.n);
        this.geometry = this.modifyPlaneGeometry(planeGeometry, data);
    },
    /**
     * Update geometry once per time interval
     * @param data ECS data
     */
    tick: function (data) {
        // TODO tick is more aggressive than necessary
        let planeGeometry = this.geometry;
        this.modifyPlaneGeometry(planeGeometry, data);
    },
    /**
     * Map distances onto UVs for hue map
     * @param dist input distance
     * @param linear metric for uv computation
     * @returns {[number,number]} uv map
     */
    hueMap: function(dist, linear = false){
        let u = 0;
        let v = 1;
        if(linear) {
            u = (dist % 5)/5; // modular division should produce a rainbow
        }else{
            u = 1-(1/(dist+1))
        }
        return([u,v]);
    },
    /**
     * Rewrite vertex matrix as buffer geometry
     * @param planeGeometry pervious frame's geometry
     * @param data ECS data
     * @returns {*} new buffergeometry with modified vertices and uv
     */
    modifyPlaneGeometry: function(planeGeometry, data){
        let l1p1 = new THREE.Vector3(data.line1point1.x, data.line1point1.y, data.line1point1.z);
        let l1p2 = new THREE.Vector3(data.line1point2.x, data.line1point2.y, data.line1point2.z);
        //let line1direction = l1p2.clone().sub(l1p1);
        let l2p1 = new THREE.Vector3(data.line2point1.x, data.line2point1.y, data.line2point1.z);
        let l2p2 = new THREE.Vector3(data.line2point2.x, data.line2point2.y, data.line2point2.z);
        let line2direction = l2p2.clone().sub(l2p1);

        for(let i=0; i<data.n+1; i++){
            const l1p = mathVec3.getProprotionalVec3(l1p1, l1p2, i/data.n);
            planeGeometry.attributes.position.setXYZ(2*i, l1p.x, l1p.y, l1p.z);
            const l2p = mathVec3.getClosestVec3OnLine(l1p, l2p1, line2direction);
            planeGeometry.attributes.position.setXYZ(2*i+1, l2p.x, l2p.y, l2p.z);
            const dist = l1p.clone().distanceTo(l2p);
            const [u, v] = this.hueMap(dist, data.linearHueMap);
            planeGeometry.attributes.uv.array[4*i] = u;
            planeGeometry.attributes.uv.array[4*i+1] = 0;
            planeGeometry.attributes.uv.array[4*i+2] = u;
            planeGeometry.attributes.uv.array[4*i+3] = 1;
        }
        planeGeometry.computeVertexNormals();
        planeGeometry.attributes.position.needsUpdate = true;
        planeGeometry.attributes.uv.needsUpdate = true;
        return(planeGeometry);
    }
});
