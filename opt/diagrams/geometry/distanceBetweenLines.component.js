const mathVec3 = require('/opt/utils/math/index');

/**
 * Maps four points onto two lines and a surface that traces the distance between the lines
 */
AFRAME.registerComponent('lines-distance-visualization', {
    schema: {
        line1pointA: {type: 'selector'},
        line1pointB: {type: 'selector'},
        line2pointA: {type: 'selector'},
        line2pointB: {type: 'selector'},
        line1: {type: 'string'},
        line2: {type: 'string'},
        surface: {type: 'string'},
        length: {type: 'number', default: 1},
        surfaceLength: {type: 'number', default:50},
        nSegments: {type:'number', default:2000},
        dynamic: {type: 'bool', default: true},
        linearHueMap: {type: 'bool', default: true}
    },
    /**
     * Initialization function creates lines and surface as children
     */
    init: function () {
        let data = this.data;  // Component property values.
        let el = this.el;  // Reference to the component's entity.
        let line1 = document.createElement('a-entity');
        let line2 = document.createElement('a-entity');
        let surface = document.createElement('a-entity');

        el.appendChild(line1);
        el.appendChild(line2);
        el.appendChild(surface);

        line1.id = (el.id + "__l1");
        line2.id = (el.id + "__l2");
        surface.id = (el.id + "__surface");

        data.line1 = line1.id;
        data.line2 = line2.id;
        data.surface = surface.id;

        document.getElementById(data.surface).setAttribute('geometry', {
            primitive: 'lineInterpolationSurface',
            linearHueMap: data.linearHueMap,
            n: 1000
        });
        document.getElementById(data.surface).setAttribute('material', {
            side: 'double',
            src: '#hueGradient',
            opacity: 0.7,
            transparent:true,
        });
        document.getElementById(data.line1).setAttribute('extended-line', {
            nSegments: data.nSegments,
            segmentLength: data.length,
            color: 'white'
        });
        document.getElementById(data.line2).setAttribute('extended-line', {
            nSegments: data.nSegments,
            segmentLength: data.length,
            color: 'white'
        });

        this.calculateGeometry(this.data)

        if (data.dynamic) {
            data.line1pointA.addEventListener('grab-hold', e => {
                this.grabhold(e)
            });
            data.line1pointA.addEventListener('grab-end', e => {
                this.grabhold(e)
            });
            data.line1pointB.addEventListener('grab-hold', e => {
                this.grabhold(e)
            });
            data.line1pointB.addEventListener('grab-end', e => {
                this.grabhold(e)
            });
            data.line2pointA.addEventListener('grab-hold', e => {
                this.grabhold(e)
            });
            data.line2pointA.addEventListener('grab-end', e => {
                this.grabhold(e)
            });
            data.line2pointB.addEventListener('grab-hold', e => {
                this.grabhold(e)
            });
            data.line2pointB.addEventListener('grab-end', e => {
                this.grabhold(e)
            });
        }
    },
    /**
     * If one of the points is held, the geometry needs to be re-assigned.
     */
    grabhold: function () {
        this.calculateGeometry(this.data)
    },
    /**
     * map data onto sub-geometries for surface and extended lines.
     * @param data ECS data
     */
    calculateGeometry: function (data) {
        const line1direction = mathVec3.getVec3DirectionFromPoints(data.line1pointA, data.line1pointB).normalize();
        const line1midpoint = mathVec3.getVec3midpointFromPoints(data.line1pointA, data.line1pointA);

        const line2direction = mathVec3.getVec3DirectionFromPoints(data.line2pointA, data.line2pointB).normalize();
        const line2midpoint = mathVec3.getVec3midpointFromPoints(data.line2pointA, data.line2pointB);

        document.getElementById(data.line1).setAttribute('extended-line', {point: line1midpoint, direction: line1direction});
        document.getElementById(data.line2).setAttribute('extended-line', {point: line2midpoint, direction: line2direction});

        const l1p1 = line1midpoint.clone().add(line1direction.clone().multiplyScalar(data.surfaceLength));
        const l1p2 = line1midpoint.clone().add(line1direction.clone().multiplyScalar(-1 * data.surfaceLength));
        const l2p1 = line2midpoint.clone().add(line2direction.clone().multiplyScalar(data.surfaceLength));
        const l2p2 = line2midpoint.clone().add(line2direction.clone().multiplyScalar(-1 * data.surfaceLength));

        document.getElementById(data.surface).setAttribute('geometry', {
            line1point1: l1p1,
            line1point2: l1p2,
            line2point1: l2p1,
            line2point2: l2p2
        });
    }
});