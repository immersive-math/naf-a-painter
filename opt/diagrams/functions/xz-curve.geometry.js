const math = require("mathjs");

AFRAME.registerGeometry('xz-curve', {
    schema: {
        fn: {type:'string', default:'f(x)=x^2+y^2'},
        n: {type: 'number', default: 100, min: 1},
        xmin: {type:'number', default: -5},
        xmax: {type:'number', default: 5},
        width: {type:'number', default: 0.01}
    },
    /**
     * initialization function
     * @param data ECS data
     */
    init: function (data) {
        this.getURLParams(data);
        let planeGeometry = new THREE.PlaneGeometry(1,1,1,data.n-1);
        this.geometry = this.modifyPlaneGeometry(planeGeometry, data);
    },
    //TODO add a tick function that watches for changes in data
    modifyPlaneGeometry: function(planeGeometry, data){
        const parser = math.parser();
        parser.evaluate(data.fn);
        const f2 = parser.get('f');

        for(let i = 0; i < data.n; i++){
            let xcoord = data.xmin + i*(data.xmax - data.xmin)/(data.n-1);
            planeGeometry.attributes.position.setXYZ(2*i, xcoord, f2(xcoord),data.width);
            planeGeometry.attributes.position.setXYZ(2*i+1, xcoord, f2(xcoord),-data.width);
        }
        planeGeometry.computeVertexNormals();
        planeGeometry.computeBoundingBox();
        planeGeometry.attributes.position.needsUpdate = true;
        return(planeGeometry);
    },
    getURLParams: function (data) {
        if(!window.location.search){return}
        let queryString = window.location.search + '&';
        console.log(queryString);

        let fn_pattern = new RegExp("(?<=(fn-curve\=))([^\?\&]+)");
        let subdiv_pattern = new RegExp("(?<=(subdiv\=))([^\?\&]+)");
        let xmin_pattern = new RegExp("(?<=(xmin\=))([^\?\&]+)");
        let xmax_pattern = new RegExp("(?<=(xmax\=))([^\?\&]+)");

        if (fn_pattern.test(queryString)) {
            data.fn = fn_pattern.exec(queryString)[0];
        }
        if (xmin_pattern.test(queryString)) {
            data.xmin = parseFloat(xmin_pattern.exec(queryString)[0]);
        }
        if (xmax_pattern.test(queryString)) {
            data.xmax = parseFloat(xmax_pattern.exec(queryString)[0]);
        }
        if (subdiv_pattern.test(queryString)) {
            data.n = parseInt(subdiv_pattern.exec(queryString)[0]);
        }
        console.log(data);
    }
});

AFRAME.registerGeometry('parameterized-curve', {
    schema: {
        fn1: {type:'string', default:'f(s,t)=s'},
        fn2: {type:'string', default:'h(s,t)=t'},
        fn3: {type:'string', default:'g(s,t)=s^2+t^2'},
        n: {type: 'number', default: 100, min: 1},
        smin: {type:'number', default: -5},
        smax: {type:'number', default: 5},
        tmin: {type:'number', default: -5},
        tmax: {type:'number', default: 5}
    },
    /**
     * initialization function
     * @param data ECS data
     */
    init: function (data) {
        this.getURLParams(data);
        let planeGeometry = new THREE.PlaneGeometry(1,1,data.n-1,data.n-1);
        this.geometry = this.modifyPlaneGeometry(planeGeometry, data);
    },
    //TODO add a tick function that watches for changes in data
    modifyPlaneGeometry: function(planeGeometry, data){
        const parser = math.parser();
        parser.evaluate(data.fn1);
        parser.evaluate(data.fn2);
        parser.evaluate(data.fn3);
        const f1 = parser.get('f');
        const f2 = parser.get('g');
        const f3 = parser.get('h');

        for(let i = 0; i < data.n; i++){
            let s = data.smin + i*(data.smax - data.smin)/(data.n-1);
                // note that Y is up in AFRAME
                planeGeometry.attributes.position.setXYZ(2*i, f1(s,t), f2(s,t)+data.width, f3(s,t));
                planeGeometry.attributes.position.setXYZ(2*i+1, f1(s,t), f2(s,t)-data.width, f3(s,t));
        }
        planeGeometry.computeVertexNormals();
        planeGeometry.computeBoundingBox();
        planeGeometry.attributes.position.needsUpdate = true;
        return(planeGeometry);
    },
    getURLParams: function (data) {
        if(!window.location.search){return}
        let queryString = window.location.search + '&';

        let fn1_pattern = new RegExp("(?<=(fn1-surface\=))([^\?\&]+)");
        let fn2_pattern = new RegExp("(?<=(fn2-surface\=))([^\?\&]+)");
        let fn3_pattern = new RegExp("(?<=(fn3-surface\=))([^\?\&]+)");
        let subdiv_pattern = new RegExp("(?<=(subdiv\=))([^\?\&]+)");
        let smin_pattern = new RegExp("(?<=(smin\=))([^\?\&]+)");
        let smax_pattern = new RegExp("(?<=(smax\=))([^\?\&]+)");
        let tmin_pattern = new RegExp("(?<=(tmin\=))([^\?\&]+)");
        let tmax_pattern = new RegExp("(?<=(tmax\=))([^\?\&]+)");

        if (fn1_pattern.test(queryString)) {
            data.fn1 = fn1_pattern.exec(queryString)[0];
        }
        if (fn2_pattern.test(queryString)) {
            data.fn2 = fn2_pattern.exec(queryString)[0];
        }
        if (fn3_pattern.test(queryString)) {
            data.fn3 = fn3_pattern.exec(queryString)[0];
        }
        if (smin_pattern.test(queryString)) {
            data.smin = parseFloat(smin_pattern.exec(queryString)[0]);
        }
        if (smax_pattern.test(queryString)) {
            data.smax = parseFloat(smax_pattern.exec(queryString)[0]);
        }
        if (tmin_pattern.test(queryString)) {
            data.tmin = parseFloat(tmin_pattern.exec(queryString)[0]);
        }
        if (tmax_pattern.test(queryString)) {
            data.tmax = parseFloat(tmax_pattern.exec(queryString)[0]);
        }
        if (subdiv_pattern.test(queryString)) {
            data.n = parseInt(subdiv_pattern.exec(queryString)[0]);
        }
        console.log(data);
    }
});