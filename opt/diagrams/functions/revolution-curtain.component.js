AFRAME.registerComponent('revolution-curtain', {
    schema: {
        curve: {type: 'string', default: 'f(x)=x^2'},
        controlPoint: {type:'selector'},
        n: {type: 'number', default: 100, min: 1},
        xmin: {type:'number', default: 0},
        xmax: {type:'number', default: 5},
        tmax: {type: 'number', default: 3},
        animation: {type: 'boolean'}
    },
    init: function() {
        this.getURLParams(this.data);

        this.el.append(this.instantiateCurve(this.data));
        this.el.append(this.instantiateAxis());

        this.instantiateSurface(this.data);

        if(this.data.animation){
            this.el.setAttribute('animation',{
                property: 'geometry.tmax',
                from: 0,
                to: 6.32,
                loop: true,
                dur: 10000
            });
        }else{
            this.data.controlPoint.addEventListener('grab-hold', e => {
                this.grabhold(this.data)
            });
        }
    },
    grabhold: function(data){
        if(!data.controlPoint){
            return;
        }
        data.tmax = getAngleFromCylindricalCoordinates(data.controlPoint, this.el);
        // TODO update geometry on-the-fly like in animation.
        // this.el.geometry.tmax=data.tmax;
        console.log(data.tmax);
    },
    instantiateSurface: function(data){
        //fnx and fny are static maps of the cylindrical coordinate system.
        const fnx = 'f(s,t) = s*cos(t)'
        const fny = 'h(s,t) = s*sin(t)'
        // fnz is defined as g(s,t) = f(s).
        const fnz = data.curve.replace('f(x)', 'g(s,t)').replaceAll('x', 's');
        console.log(fnz)
        this.el.setAttribute('geometry', {
            primitive: 'parameterized-surface',
            fn1: fnx,
            fn2: fny,
            fn3: fnz,
            n: data.n,
            smin: data.xmin,
            smax: data.xmax,
            tmin: 0,
            tmax: data.tmax
        });
        this.el.setAttribute('material',{
                color: 'green',
                transparent:true,
                opacity:0.2,
                side: 'double'
            });
    },
    instantiateCurve: function(data){
        let curve_el = document.createElement('a-entity');
        curve_el.setAttribute('geometry', {
            primitive: 'xz-curve',
            fn: data.curve,
            n: data.n,
            xmin: data.xmin,
            xmax: data.xmax
        });
        curve_el.setAttribute('material',{
            color: 'red',
            transparent:true,
            opacity: 0.8,
            side: 'double'
        });
        curve_el.id = this.el.id + '-curve';
        return curve_el;
    },
    instantiateAxis: function(){
        let axis_el = document.createElement('a-entity')
        axis_el.setAttribute('line',
            {
                start: {x: 0, y:-5 , z:0},
                end: {x:0 ,y:20, z:0},
                color: 'blue'
            }
            )
        axis_el.id = this.el.id + '-axis';
        return axis_el;
    },
    getURLParams: function (data) {
        if(!window.location.search){return}
        let queryString = window.location.search + '&';

        let fn_curtain = new RegExp("(?<=(fn-curtain\=))([^\?\&]+)");
        let xmin_pattern = new RegExp("(?<=(xmin\=))([^\?\&]+)");
        let xmax_pattern = new RegExp("(?<=(xmax\=))([^\?\&]+)");

        if (fn_curtain.test(queryString)) {
            data.curve = fn_curtain.exec(queryString)[0];
        }
        if (xmin_pattern.test(queryString)) {
            data.xmin = parseFloat(xmin_pattern.exec(queryString)[0]);
        }
        if (xmax_pattern.test(queryString)) {
            data.xmax = parseFloat(xmax_pattern.exec(queryString)[0]);
        }
    }
});

/**
 * Find direction between two points (aframe elements)
 * @param pointa aframe element
 * @param pointb aframe element
 * @returns {*} Three.Vector3 normalized displacement
 */
function getVec3DirectionFromPoints(pointa, pointb){
    const posa = getVec3position(pointa);
    const posb = getVec3position(pointb);
    return(posa.clone().sub(posb).normalize());
}

/**
 * Returns an elmenets position represented as a Three.Vector3
 * @param el aframe element
 * @returns {*} Three.Vector3 local position
 */
function getVec3position(el){
    const pos = el.getAttribute('position');
    return(new THREE.Vector3(pos.x, pos.y, pos.z));
}

/**
 * Returns an elmenets position represented as a Three.Vector3
 * @param el aframe element
 * @returns {*} Three.Vector3 local position
 */
function getVec3rotation(el){
    const rot = el.getAttribute('rotation');
    return(new THREE.Euler(rot.x, rot.y, rot.z));
}

function getAngleFromCylindricalCoordinates(point, reference){
    let axis = new THREE.Vector3(0,1,0);
    axis.applyEuler(getVec3rotation(reference));
    let start = new THREE.Vector3(1,0,0);
    start.applyEuler(getVec3rotation(reference));
    let controlDir = getVec3DirectionFromPoints(point, reference);

    return controlDir.clone().projectOnPlane(axis).angleTo(start);
}