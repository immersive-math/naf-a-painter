const math = require("mathjs");

AFRAME.registerGeometry('xyz-surface', {
    schema: {
        fn: {type:'string', default:'f(x,y)=x^2+y^2'},
        n: {type: 'number', default: 100, min: 1},
        xmin: {type:'number', default: -5},
        xmax: {type:'number', default: 5},
        ymin: {type:'number', default: -5},
        ymax: {type:'number', default: 5}
    },
    /**
     * initialization function
     * @param data ECS data
     */
    init: function (data) {
        //TODO move url parameters into a component?
        this.getURLParams(data);
        let planeGeometry = new THREE.PlaneGeometry(1,1,data.n-1,data.n-1);
        this.geometry = this.modifyPlaneGeometry(planeGeometry, data);
    },
    //TODO add a tick function that watches for changes in data
    modifyPlaneGeometry: function(planeGeometry, data){
        const parser = math.parser();
        parser.evaluate(data.fn);
        const f2 = parser.get('f');

        for(let i = 0; i < data.n; i++){
            let xcoord = data.xmin + i*(data.xmax - data.xmin)/(data.n-1);
            for(let j = 0; j < data.n; j++){
                let zcoord = data.ymin + j*(data.ymax - data.ymin)/(data.n-1);
                // note that Y is up in AFRAME
                planeGeometry.attributes.position.setXYZ(data.n*i+j, xcoord, f2(xcoord,zcoord),zcoord);
            }
        }
        planeGeometry.computeVertexNormals();
        planeGeometry.computeBoundingBox();
        planeGeometry.attributes.position.needsUpdate = true;
        return(planeGeometry);
    },
    getURLParams: function (data) {
        if(!window.location.search){return}
        let queryString = window.location.search + '&';

        let fn_pattern = new RegExp("(?<=(fn-surface\=))([^\?\&]+)");
        let subdiv_pattern = new RegExp("(?<=(subdiv\=))([^\?\&]+)");
        let xmin_pattern = new RegExp("(?<=(xmin\=))([^\?\&]+)");
        let xmax_pattern = new RegExp("(?<=(xmax\=))([^\?\&]+)");
        let ymin_pattern = new RegExp("(?<=(ymin\=))([^\?\&]+)");
        let ymax_pattern = new RegExp("(?<=(ymax\=))([^\?\&]+)");

        if (fn_pattern.test(queryString)) {
            data.fn = fn_pattern.exec(queryString)[0];
        }
        if (xmin_pattern.test(queryString)) {
            data.xmin = parseFloat(xmin_pattern.exec(queryString)[0]);
        }
        if (xmax_pattern.test(queryString)) {
            data.xmax = parseFloat(xmax_pattern.exec(queryString)[0]);
        }
        if (ymin_pattern.test(queryString)) {
            data.ymin = parseFloat(ymin_pattern.exec(queryString)[0]);
        }
        if (ymax_pattern.test(queryString)) {
            data.ymax = parseFloat(ymax_pattern.exec(queryString)[0]);
        }
        if (subdiv_pattern.test(queryString)) {
            data.n = parseInt(subdiv_pattern.exec(queryString)[0]);
        }
        console.log(data);
    }
});


AFRAME.registerGeometry('parameterized-surface', {
    schema: {
        fn1: {type:'string', default:'f(s,t)=s'},
        fn2: {type:'string', default:'h(s,t)=t'},
        fn3: {type:'string', default:'g(s,t)=s^2+t^2'},
        n: {type: 'number', default: 100, min: 1},
        smin: {type:'number', default: -5},
        smax: {type:'number', default: 5},
        tmin: {type:'number', default: -5},
        tmax: {type:'number', default: 5}
    },
    /**
     * initialization function
     * @param data ECS data
     */
    init: function (data) {
        this.getURLParams(data);
        let planeGeometry = new THREE.PlaneGeometry(1,1,data.n-1,data.n-1);
        this.geometry = this.modifyPlaneGeometry(planeGeometry, data);
    },
    //TODO add a tick function that watches for changes in data
    modifyPlaneGeometry: function(planeGeometry, data){
        const parser = math.parser();
        parser.evaluate(data.fn1);
        parser.evaluate(data.fn2);
        parser.evaluate(data.fn3);
        const f1 = parser.get('f');
        const f2 = parser.get('g');
        const f3 = parser.get('h');

        for(let i = 0; i < data.n; i++){
            let s = data.smin + i*(data.smax - data.smin)/(data.n-1);
            for(let j = 0; j < data.n; j++){
                let t = data.tmin + j*(data.tmax - data.tmin)/(data.n-1);
                // note that Y is up in AFRAME
                planeGeometry.attributes.position.setXYZ(data.n*i+j, f1(s,t), f2(s,t), f3(s,t));
            }
        }
        planeGeometry.computeVertexNormals();
        planeGeometry.computeBoundingBox();
        planeGeometry.attributes.position.needsUpdate = true;
        return(planeGeometry);
    },
    getURLParams: function (data) {
        if(!window.location.search){return}
        let queryString = window.location.search + '&';

        let fn1_pattern = new RegExp("(?<=(fn1-surface\=))([^\?\&]+)");
        let fn2_pattern = new RegExp("(?<=(fn2-surface\=))([^\?\&]+)");
        let fn3_pattern = new RegExp("(?<=(fn3-surface\=))([^\?\&]+)");
        let subdiv_pattern = new RegExp("(?<=(subdiv\=))([^\?\&]+)");
        let smin_pattern = new RegExp("(?<=(smin\=))([^\?\&]+)");
        let smax_pattern = new RegExp("(?<=(smax\=))([^\?\&]+)");
        let tmin_pattern = new RegExp("(?<=(tmin\=))([^\?\&]+)");
        let tmax_pattern = new RegExp("(?<=(tmax\=))([^\?\&]+)");

        if (fn1_pattern.test(queryString)) {
            data.fn1 = fn1_pattern.exec(queryString)[0];
        }
        if (fn2_pattern.test(queryString)) {
            data.fn2 = fn2_pattern.exec(queryString)[0];
        }
        if (fn3_pattern.test(queryString)) {
            data.fn3 = fn3_pattern.exec(queryString)[0];
        }
        if (smin_pattern.test(queryString)) {
            data.smin = parseFloat(smin_pattern.exec(queryString)[0]);
        }
        if (smax_pattern.test(queryString)) {
            data.smax = parseFloat(smax_pattern.exec(queryString)[0]);
        }
        if (tmin_pattern.test(queryString)) {
            data.tmin = parseFloat(tmin_pattern.exec(queryString)[0]);
        }
        if (tmax_pattern.test(queryString)) {
            data.tmax = parseFloat(tmax_pattern.exec(queryString)[0]);
        }
        if (subdiv_pattern.test(queryString)) {
            data.n = parseInt(subdiv_pattern.exec(queryString)[0]);
        }
        console.log(data);
    }
});