const THREE = window.THREE
    ? window.THREE // Prefer consumption from global THREE, if exists
    : {
        Color,
        LineBasicMaterial,
        LineSegments,
        Mesh,
        MeshPhongMaterial,
        SphereGeometry,
        TextureLoader
    };
const axis = new THREE.Vector3(0,1,0).applyEuler(new THREE.Euler(0, THREE.MathUtils.degToRad(23.5), 0));

const reanalyzer_daily_maps = {
        url: 'https://climatereanalyzer.org/reanalysis/daily_maps/clim_frames/$SET/world-ced2/$YYYY/$SET_world-ced_$YYYY_d$DDD.png',
        sets: ['t2anom', 't2max', 't2min', 't2max', 'sstanom', 'sst'],
}

AFRAME.registerComponent('climate-reanalyzer', {
    dependencies: ['globe'],
    schema:{
        globeImageUrl: {type: 'string', default: 'https://climatereanalyzer.org/reanalysis/daily_maps/clim_frames/$SET/$REGION/$YYYY/$SET_$REGION_$YYYY_d$DDD.png'},
        year: {type: 'number', default: 2021, min: 1979, max: 2022},
        day: {type: 'number', default: 1, min: 1, max: 365},
        dataset: {type: 'string', default: 'sst'},
        region: {type: 'string', default: 'world-ced2'},
        umin: {type:'number', default:0.0372727272727273, min: 0, max: 1},
        umax: {type:'number', default:0.983636363636364, min: 0, max: 1},
        vmin: {type:'number', default:0.172914147521161, min: 0, max: 1},
        vmax: {type:'number', default:0.927448609431681, min: 0, max: 1},
        fps: {type:'number', default: 15, min:1, max:120},
        text: {type: 'selector'},
        legend: {type:'selector'},
        fixAxis: {type: 'boolean', default:false},
        controlCenter: {type: 'selector'},
        controlNorth: {type: 'selector'},
        controlMeridian: {type: 'selector'},
        animated: {type:'boolean', default:true}
    },
    init: function(){
        this.getURLParams(this.data);
        this.el.sceneEl.querySelector('#spectator-camera').setAttribute('position', "0 1.2 1.5");
        this.el.sceneEl.querySelector('#spectator-camera').setAttribute('wasd-controls', 'fly', true);
        const uvtesturl='https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/UV_checker_Map_byValle.jpg/1280px-UV_checker_Map_byValle.jpg';
        this.el.setAttribute('globe', {globeImageUrl: uvtesturl});
        let endDay = 365;
        if(this.data.year = 2022)
        {
            endDay = 268;
        }
        this.el.setAttribute('animation__day', {
            'property': 'climate-reanalyzer.day',
            'easing':'linear',
            'from': 1,
            'to': endDay,
            'dur': Math.floor(365000/this.data.fps),
            'loop': true,
            'round': true
        });
        this.legendNeedsUpdate = true;
        // this.el.setAttribute('animation__year', {
        //     'property': 'climate-reanalyzer.year',
        //     'easing':'linear',
        //     'from': 1979,
        //     'to': 2022,
        //     'dur': 36500*23,
        //     'loop': true,
        //     'round': true
        // });
        this.data.controlCenter.addEventListener('grab-hold', e => {this.grabhold(e)});
        this.data.controlCenter.addEventListener('grab-end', e => {this.grabhold(e)});
        this.data.controlNorth.addEventListener('grab-hold', e => {this.grabhold(e)});
        this.data.controlNorth.addEventListener('grab-end', e => {this.grabhold(e)});
    },
    update: function(oldData){
      this.updateGlobeTexture(this.data);
      this.updateLegendTexture(this.data);
      this.data.text.setAttribute('text', 'value', 'SST: 2022_d' + String(this.data.day).padStart(3, '0'));
    },
    grabhold: function(){
        this.el.setAttribute('position', this.data.controlCenter.getAttribute('position'));
        this.scale(this.data);
    },
    remoteURL: function(data){
        const day = String(data.day).padStart(3, '0');
        const year = String(data.year);
        const url = data.globeImageUrl.replaceAll('$SET', data.dataset).replaceAll('$REGION', data.region).replaceAll('$YYYY', year).replaceAll('$DDD', day);
        return url;
    },
    updateGlobeTexture: function(data){
        let globeComponent = this.el.components.globe;
        let globeThree = globeComponent.globe;
        const mat = globeThree.globeMaterial();
        new THREE.TextureLoader().load(this.remoteURL(data), texture => {
            texture.repeat.setX(data.umax-data.umin); // adjusts horizontal repeat
            texture.repeat.setY(data.vmax-data.vmin); //adjusts vertical repeat.
            texture.offset.setX(data.umin);
            texture.offset.setY(data.vmin);
            mat.map = texture;
            mat.side=THREE.DoubleSide;
        })
    },
    updateLegendTexture: function(data){
        const mat2 = data.legend.object3D.el.object3DMap.mesh.material;
        console.log(mat2);
        new THREE.TextureLoader().load(this.remoteURL(data), texture => {
            texture.repeat.setX(1.0); // adjusts horizontal repeat
            texture.repeat.setY(0.14); //adjusts vertical repeat.
            texture.offset.setX(0);
            texture.offset.setY(0);
            mat2.map = texture;
            mat2.color = null;
        });
    },
    getURLParams: function (data) {
         if (!window.location.search) {
             return
         }
         let queryString = window.location.search + '&';
         console.log(queryString);

         let texture_pattern = new RegExp("(?<=(texture\=))([^\?\&]+)");
         let dataset_pattern = new RegExp("(?<=(dataset\=))([^\?\&]+)");
        let region_pattern = new RegExp("(?<=(region\=))([^\?\&]+)");
         let year_pattern = new RegExp("(?<=(year\=))([^\?\&]+)");
         let umin_pattern = new RegExp("(?<=(umin\=))([^\?\&]+)");
         let umax_pattern = new RegExp("(?<=(umax\=))([^\?\&]+)");
         let vmin_pattern = new RegExp("(?<=(vmin\=))([^\?\&]+)");
         let vmax_pattern = new RegExp("(?<=(vmax\=))([^\?\&]+)");

         if (texture_pattern.test(queryString)) {
             data.globeImageUrl = texture_pattern.exec(queryString)[0];
         }
        if (dataset_pattern.test(queryString)) {
            data.dataset = dataset_pattern.exec(queryString)[0];
        }
        if (region_pattern.test(queryString)) {
            data.region = region_pattern.exec(queryString)[0];
        }
        if (year_pattern.test(queryString)) {
            data.year = year_pattern.exec(queryString)[0];
        }
         if (umin_pattern.test(queryString)) {
             data.umin = umin_pattern.exec(queryString)[0];
         }
         if (umax_pattern.test(queryString)) {
             data.umax = umax_pattern.exec(queryString)[0];
         }
         if (vmin_pattern.test(queryString)) {
             data.vmin = vmin_pattern.exec(queryString)[0];
         }
         if (vmax_pattern.test(queryString)) {
             data.vmax = vmax_pattern.exec(queryString)[0];
         }
    },
    scale: function(data){
        const center = getVec3position(data.controlCenter);
        const north = getVec3position(data.controlNorth);
        const meridian = getVec3position(data.controlMeridian);
        const radius = north.clone().distanceTo(center);
        
        if (radius > 0) {
            this.el.setAttribute('scale', {x: radius/100, y: radius/100, z: radius/100});
            if(data.fixAxis) {
                //move north pole
                const north_new = axis.clone().multiplyScalar(radius).add(center);
                data.controlNorth.setAttribute('position', {x: north_new.x, y: north_new.y, z: north_new.z});
            }else{
                //determine rotation for axis
                const northDirection = north.clone().sub(center).normalize();
                const meridianDirection = meridian.clone().sub(center).projectOnPlane(northDirection).normalize();

                let q = new THREE.Quaternion();
                q.setFromUnitVectors(new THREE.Vector3(0,1,0), northDirection);

                let v = new THREE.Vector3(1,0,0);
                v.applyQuaternion(q);

                // correct absolute value of angle.
                const cross = v.clone().cross(meridianDirection);
                const angle = v.clone().angleTo(meridianDirection)*Math.sign(northDirection.clone().dot(cross));

                let meridianQ = new THREE.Quaternion();
                meridianQ.setFromAxisAngle(northDirection, angle);

                q.premultiply(meridianQ);

                // let rotationMatrix = new THREE.Matrix4();
                // rotationMatrix.makeBasis(meridianDirection, northDirection, northDirection.clone().cross(meridianDirection));

                let globeComponent = this.el.components.globe;
                let globeThree = globeComponent.globe;
                globeThree.setRotationFromQuaternion(q);
                //move meridian
                const meridian_new = meridianDirection.multiplyScalar(radius).add(center);
                data.controlMeridian.setAttribute('position', {x: meridian_new.x, y: meridian_new.y, z: meridian_new.z});
            }
        }
    }
});

/**
 * Find direction between two points (aframe elements)
 * @param pointa aframe element
 * @param pointb aframe element
 * @returns {*} Three.Vector3 normalized displacement
 */
function getVec3DirectionFromPoints(pointa, pointb){
    const posa = getVec3position(pointa);
    const posb = getVec3position(pointb);
    return(posa.clone().sub(posb).normalize());
}


/**
 * Returns an elmenets position represented as a Three.Vector3
 * @param el aframe element
 * @returns {*} Three.Vector3 local position
 */
function getVec3position(el){
    const pos = el.getAttribute('position');
    return(new THREE.Vector3(pos.x, pos.y, pos.z));
}

function calculateSphereRotation(northDir, meridianDir){
    let quaternion = new THREE.Quaternion();

    return quaternion;
}