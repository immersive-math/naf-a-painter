// Temporary workaround for template declaration; see issue 167
NAF.schemas.getComponentsOriginal = NAF.schemas.getComponents;

/**
 * Adds avatar and points to networked-aframe-schema schema. Update this to include other basic geometry objects
 * @param template
 * @returns {*}
 */
NAF.schemas.getComponents = (template) => {
    if (!NAF.schemas.hasTemplate('#avatar-template')) {
        NAF.schemas.add({
            template: '#avatar-template',
            components: [
                // position and rotation are added by default if we don't include a template, but since
                // we also want to sync the color, we need to specify a custom template; if we didn't
                // include position and rotation in this custom template, they'd not be synced.
                {
                    component: 'position'
                },
                {
                    component: 'rotation',
                },
                // this is how we sync a particular property of a particular component for a particular
                // child element of template instances.
                {
                    selector: '.head',
                    component: 'material',
                    property: 'color' // property is optional; if excluded, syncs everything in the component schema
                }
            ]
        });
    }
    if (!NAF.schemas.hasTemplate("#fixed-point-template")) {
        NAF.schemas.add({
            template: '#fixed-point-template',
            components: [
                {
                    component: 'position',
                    requiresNetworkUpdate: NAF.utils.vectorRequiresUpdate(0.001)
                }
            ]
        });
    }
    if (!NAF.schemas.hasTemplate("#interactable-point-template")) {
        NAF.schemas.add({
            template: '#interactable-point-template',
            components: [
                {
                    component: 'position',
                    requiresNetworkUpdate: NAF.utils.vectorRequiresUpdate(0.001)
                },
                {
                    component: 'point',
                    property: 'grabbed'
                }
            ]
        });
    }
    return NAF.schemas.getComponentsOriginal(template);
};

AFRAME.registerComponent('avatar',
    {
        init: function() {
            this.el.object3D.rotation.order = "YXZ";
        }
    });