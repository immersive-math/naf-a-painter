// Temporary workaround for template declaration; see issue 167
NAF.schemas.getComponentsOriginal = NAF.schemas.getComponents;

/**
 * Add avatar to Networked Aframe Schema
 * @param template
 * @returns {*}
 */
NAF.schemas.getComponents = (template) => {
    if (!NAF.schemas.hasTemplate('#avatar-template')) {
        NAF.schemas.add({
            template: '#avatar-template',
            components: [
                // position and rotation are added by default if we don't include a template, but since
                // we also want to sync the color, we need to specify a custom template; if we didn't
                // include position and rotation in this custom template, they'd not be synced.
                {
                    component: 'position'
                },
                {
                    component: 'rotation',
                },
                // this is how we sync a particular property of a particular component for a particular
                // child element of template instances.
                {
                    selector: '.head',
                    component: 'material',
                    property: 'color' // property is optional; if excluded, syncs everything in the component schema
                }
            ]
        });
    }
    NAF.schemas.add({
        template: '#left-controller-default-template',
        components: [
            {
                component: 'position',
                requiresNetworkUpdate: NAF.utils.vectorRequiresUpdate(0.001)
            },
            {
                component: 'rotation',
                requiresNetworkUpdate: NAF.utils.vectorRequiresUpdate(0.5)
            },
            'networked-hand-controls'
        ]
    });
    NAF.schemas.add({
        template: '#right-controller-default-template',
        components: [
            {
                component: 'position',
                requiresNetworkUpdate: NAF.utils.vectorRequiresUpdate(0.001)
            },
            {
                component: 'rotation',
                requiresNetworkUpdate: NAF.utils.vectorRequiresUpdate(0.5)
            },
            'networked-hand-controls'
        ]
    });

    const components = NAF.schemas.getComponentsOriginal(template);
    return components;
};


AFRAME.registerComponent('avatar',
{
    init: function() {
        this.el.object3D.rotation.order = "YXZ";
    }
});