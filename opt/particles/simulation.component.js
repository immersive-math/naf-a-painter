AFRAME.registerComponent('particle-simulation', {
    schema: {
        url: {type: 'string', default:  'https://raw.githubusercontent.com/supermedium/aframe-particleplayer-component/master/examples/10000/10000.json'}
    },
    init: function(){
        //TODO fetch url parameters, make query to database
        this.instantiateParticleSystem(this.data);
    },
    instantiateParticleSystem: function(data){
        this.el.setAttribute('particleplayer', {
            src: '#particles',
            texture: '#particle-tex',
            dur: 10000,
            count: '10%',
            scale: 1,
            on: 'init',
            loop: 'true',
            color: 'red',
            pscale: 0.1,
            interpolate: true,
            loop: true
        });
    },
});