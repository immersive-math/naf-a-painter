/* global AFRAME, NAF, THREE */
/**
 * Reassign ownership when grasping for networked-aframe
 * TODO - fix bug where ownership can't be gained without two clients
 */
AFRAME.registerComponent('grab-ownership', {
    schema: {
        grabbed:{type:'boolean'}
    },

    init: function() {
        const el = this.el;  // Reference to the component's entity.
        const data = this.data;

        //onConnect
        this.el.addEventListener('onConnect', function(){el.emit('grab-hold', {el: el});})
        this.el.addEventListener('clientConnected', function(){el.emit('grab-hold', {el: el});})
        this.el.addEventListener('grabstarted', function() {
            if(NAF.utils.takeOwnership(el)){
                data.grabbed = true;
            }
            if(NAF.utils.isMine(el)) {
                data.grabbed = true;
            }
            AFRAME.log('object grabbed');
        });
        this.el.addEventListener('grabended', function() {
            data.grabbed = false;
            AFRAME.log('object released');
        });

        NAF.utils.getNetworkedEntity(el).then((el) => {
            // Opacity is not a networked attribute, but change it based on ownership events
            let timeout;

            this.el.addEventListener('ownership-changed', e => {
                clearTimeout(timeout);
                AFRAME.log(e.detail)
            });
        });
    },
    tick: function(){
        //TODO optimize this to not run on every frame
        if(this.data.grabbed){
            this.el.emit('grab-hold', {el: this.el});
            //AFRAME.log("this object is going for a walk!");
        }
    }
});