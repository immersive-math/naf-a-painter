//inspired by https://medium.com/swlh/creating-a-vr-audio-visual-experience-on-the-web-with-a-frame-and-tone-js-3800ed305a97
import * as Tone from 'tone';

const polySynth = new Tone.PolySynth(Tone.Synth, {
    volume: -15, // -15dB
    oscillator: {
        type: 'triangle' // triangle wave
    },
    envelope: {
        attack: 0.03, // 30ms attack
        release: 1 // 1s release
    }
}).toDestination();

AFRAME.registerComponent('polysynth', {
    schema: {
        // Describe the property of the component.
        notes: {
            // NOTE: type: 'array' is referring to our Aframe component's property type, not a synth preset
            type: 'array',
            default: ['C4', 'E4', 'D4'] // Cmaj chord
        },
        duration: {
            type: 'string',
            default: '4n' // fourth note default time
        }
    },
    init: function () {
        // Do something when component first attached.
        this.el.addEventListener('polysynth-tone', this.trigger.bind(this))
        this.trigger();
    },
    trigger: function(){
        for (let i = 0; i < this.data.notes.length; i++){
            polySynth.triggerAttackRelease(this.data.notes[i], this.data.duration)
        }

    }
});