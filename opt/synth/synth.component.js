//inspired by https://medium.com/swlh/creating-a-vr-audio-visual-experience-on-the-web-with-a-frame-and-tone-js-3800ed305a97
import * as Tone from 'tone';

const synth = new Tone.Synth({
    volume: -15, // -15dB
    oscillator: {
        type: 'triangle' // triangle wave
    },
    envelope: {
        attack: 0.03, // 30ms attack
        release: 1 // 1s release
    }
}).toDestination();

AFRAME.registerComponent('synth', {
    schema: {
        // Describe the property of the component.
        note: {
            // NOTE: type: 'string' is referring to our Aframe component's property type, not a synth preset
            type: 'string',
            default: 'C4' // C4 default note
        },
        duration: {
            type: 'string',
            default: '4n' // fourth note default time
        }
    },
    init: function () {
        // Do something when component first attached.
        this.el.addEventListener('synth-tone', this.trigger.bind(this))
        this.trigger();
    },
    trigger: function(){
        synth.triggerAttackRelease(this.data.note, this.data.duration)
    }
});