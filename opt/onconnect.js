/**
 * Runs when scene connects to Networked-aframe server
 * TODO - fix bug where ownership can't be gained without two clients
 */
function onConnect () {
    console.log("onConnect", new Date());

    const sceneEl = document.querySelector('a-scene');
    const els = sceneEl.querySelectorAll('.point')
    for (var i = 0; i < els.length; i++) {
        els[i].emit('onConnect', {el: els[i]})
        console.log(els[i]);
    }
    // TODO consider emitting event here to get each point to be graspable (fix bug)
    // TODO consider emitting event (grab-hold) for one frame to redraw dependent figures.
}
