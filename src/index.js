window.saveAs = require('../vendor/saveas.js').saveAs;

require('networked-aframe');
require('aframe-rounded');
require('aframe-blink-controls');
require('aframe-tooltip-component');
require('aframe-extras');
require('aframe-environment-component');
require('aframe-randomizer-components');
require('clipboard');
require('hammerjs');
require('pressure');
require('aframe-haptics-component');
require('aframe-log-component');

require('./atlas.js');
require('./dragndrop.js');
require('./binarymanager.js');
require('./sharedbuffergeometrymanager.js');
require('./sharedbuffergeometry.js');
require('./mappings.js');

require('./utils.js');
require('./ui2d.js');

require('./systems/brush.js');
require('./systems/ui.js');
require('./systems/painter.js');

require('./components/index');
require('./brushes/index');

require('./systems/sync.js');