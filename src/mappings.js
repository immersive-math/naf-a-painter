require('../vendor/aframe-input-mapping-component.js')

const mappings = {
    behaviours: {},
    mappings: {
        //hand-tracking controls are currently not compatible with the paint brush system.
        //TODO rewrite the brush component for compatibility
        painting: {
            common: {
                //'grip.down': 'undo',
                'trigger.changed': 'paint'
            },

            'vive-controls': {
                //'axismove': 'changeBrushSizeInc',
                //'trackpad.touchstart': 'startChangeBrushSize',
                'menu.down': 'toggleMenu',

                // Teleport
                'trackpad.down': 'aim',
                'trackpad.up': 'teleport'
            },

            'oculus-touch-controls': {
                //'axismove': 'changeBrushSizeAbs',
                'abutton.down': 'toggleMenu',
                'xbutton.down': 'toggleMenu',

                // Teleport
                'ybutton.down': 'aim',
                'ybutton.up': 'teleport',
                'bbutton.down': 'aim',
                'bbutton.up': 'teleport',
                'thumbstick.down' : 'aim',
                'thumbstick.up' : 'teleport',
                'grip.down': 'graspstarted',
                'grip.up': 'graspended',
                'grip.changed': 'graspmoved'
            },

            'windows-motion-controls': {
                'axismove': 'changeBrushSizeAbs',
                'menu.down': 'toggleMenu',

                // Teleport
                'trackpad.down': 'aim',
                'trackpad.up': 'teleport'
            },

            'valve-index-controls': {
                //'axismove': 'changeBrushSizeInc',
                //'trackpad.touchstart': 'startChangeBrushSize',
                'abutton.down': 'toggleMenu',

                // Teleport
                'thumbstick.down': 'aim',
                'thumbstick.up': 'teleport',
                'bbutton.down': 'aim',
                'bbutton.up': 'teleport'

            }
        }
    }
};
AFRAME.registerInputMappings(mappings);
AFRAME.currentInputMapping = 'painting';