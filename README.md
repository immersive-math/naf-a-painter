# Multi-User, Networked A-Painter

[Try It!](https://apainter.camdenbock.dev/
Please note - the [EasyRTC server](https://gitlab.com/immersive-math/networked-aframe-server) is self-hosted and might not always be live.

Video Examples:

[https://video.maine.edu/playlist/dedicated/1_g04z9hqv/](https://video.maine.edu/playlist/dedicated/1_g04z9hqv/)

## Forks A-Painter

Paint in VR in your browser. [Read more!](https://blog.mozvr.com/a-painter/)

[![A-Painter logo](https://blog.mozvr.com/content/images/2016/09/logo_a-painter_high-nobrands.jpg)](https://blog.mozvr.com/a-painter/)

## Built with Networked A-Frame
[![Networked A-Frame](https://camo.githubusercontent.com/6ac6f19be15a423d93da68466f14293cbe79b5255d437063636041d740b7cf5b/687474703a2f2f692e696d6775722e636f6d2f376464624530712e676966)](https://github.com/networked-aframe/networked-aframe)

## Fork this repository
To fork this repository and host your own instance:
 - Choose a [NAF Adapter and Hosting Method](https://github.com/networked-aframe/networked-aframe/blob/master/docs/hosting-networked-aframe-on-a-server.md)
 - Set ServerURL and and Adapter in [Apainter.njk](https://gitlab.com/immersive-math/naf-a-painter/-/blob/main/src/templates/apainter.njk?ref_type=heads) to point to your adapter.

## Up-Stream
Non diagramming content from this repository with eventually be contributed up-stream to [Haden Jame's Lee's Networked-A-Painter](https://github.com/haydenjameslee/networked-a-painter).
