const gulp = require('gulp');
const nunjucksRenderer = require('gulp-nunjucks-render')

gulp.task('headers', function(){
    return gulp
        .src('_headers')
        .pipe(gulp.dest('public/'))
});

gulp.task('examples_xr', function(){
    var manageEnvironment = function(environment) {
        environment.addFilter('slug', function(str) {
            return str && str.replace(/\s/g, '-', str).toLowerCase();
        });

        environment.addGlobal('spectator', false);
        environment.addGlobal('easyrtc_server', 'easyrtc.camdenbock.dev');
        environment.addGlobal('timestamp', new Date().toISOString());
    }
    return gulp
        .src('examples/**.njk')
        .pipe(
            nunjucksRenderer({
                path: ["src/templates", "examples/diagrams"],
                manageEnv: manageEnvironment
            })
        )
        .pipe(gulp.dest('public/'))
});

gulp.task('examples_spectator', function(){
    var manageEnvironment = function(environment) {
        environment.addFilter('slug', function(str) {
            return str && str.replace(/\s/g, '-', str).toLowerCase();
        });

        environment.addGlobal('spectator', true);
        environment.addGlobal('easyrtc_server', 'easyrtc.camdenbock.dev');
        environment.addGlobal('timestamp', new Date().toISOString());
    }
    return gulp
        .src('examples/**.njk')
        .pipe(
            nunjucksRenderer({
                path: ["src/templates", "examples/diagrams"],
                manageEnv: manageEnvironment
            })
        )
        .pipe(gulp.dest('public/spectator/'))
});

gulp.task('assets', function (){
    return gulp
        .src('assets/**')
        .pipe(gulp.dest('public/assets/'))
})

gulp.task('brushes', function (){
    return gulp
        .src('brushes/**')
        .pipe(gulp.dest('public/brushes'))
})

gulp.task('css', function (){
    return gulp
        .src('css/**')
        .pipe(gulp.dest('public/css'))
})

gulp.task('img', function (){
    return gulp
        .src('img/**')
        .pipe(gulp.dest('public/img'))
})

gulp.task('paintings', function (){
    return gulp
        .src('paintings/**')
        .pipe(gulp.dest('public/paintings'))
})

gulp.task('sounds', function (){
    return gulp
        .src('sounds/**')
        .pipe(gulp.dest('public/sounds'))
})

gulp.task('aframe', function(){
    return gulp
        .src('node_modules/aframe/dist/**')
        .pipe(gulp.dest('public/aframe/'))
})

gulp.task('schema', function(){
    return gulp
        .src('opt/networked-aframe-schema/**')
        .pipe(gulp.dest('public/naf-schema/'))
})